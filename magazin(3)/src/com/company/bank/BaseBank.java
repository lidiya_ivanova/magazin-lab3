package com.company.bank;

import com.company.interfaces.IBank;

public class BaseBank implements IBank {
    private String name;
    private String creditDescription;

    public BaseBank(String name, String creditDescription) {
        this.name = name;
        this.creditDescription = creditDescription;
    }

    public void checkInfo() {}

    public void giveCredit() {}

    public String getName() {
        return name;
    }

    public String getCreditDescription() {
        return creditDescription;
    }
}


