package com.company.goods;

import com.company.interfaces.IDepartment;

public class HardDrive extends ElectronicDevice{
    private String volume;

    public HardDrive(IDepartment department, String name, boolean hasGuarantee, String price, String company, String volume) {
        super(department, name, hasGuarantee, price, company);
        this.volume = volume;
    }

    public void format() {}

    public void copy() {}

    public void delete() {}
}

