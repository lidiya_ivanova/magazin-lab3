package com.company.goods;

import com.company.departments.BaseDepartment;
import com.company.interfaces.IDepartment;
import com.company.interfaces.IGood;

public abstract class BaseGoods implements IGood {
    private IDepartment department;
    private String name;
    private boolean hasGuarantee;
    private String price;
    private String company;

    public BaseGoods(IDepartment department, String name, boolean hasGuarantee, String price, String company) {
        this.department = department;
        this.name = name;
        this.hasGuarantee = hasGuarantee;
        this.price = price;
        this.company = company;
    }

    public BaseGoods(String name) {
        this.name=name;
    }

    @Override
    public IDepartment getDepartment() {
        return department;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isHasGuarantee() {
        return hasGuarantee;
    }

    @Override
    public String getPrice() {
        return price;
    }

    @Override
    public String getCompany() {
        return company;
    }
}


